#pragma once
class Player: public GameObject
{
	public:
		ofImage expr[3];
			//Contains alternate character sprite images in an array
		int currentExpr;
		ofImage skins[5];
		bool changeSkin;
		//Contains images for the accessories or clothes that the characters may wear
		int currentSkin;
		int score;
		int colType; // 0 = no collision, 1 = positive object, 2 = bad object collision

		float animTime;

		Player();
};

