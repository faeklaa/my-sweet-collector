#pragma once

#include "ofMain.h"
#include "main.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		GameController Game;

		//static game stuff
		ofImage background;
		ofTrueTypeFont numFont;
		ofTrueTypeFont textFont;
		ofImage scoreBask;
		ofImage logo;

		ofImage pinoCtrls;
		ofImage koriCtrls;

		GameObject startBtn;
		GameObject replayBtn;

		Player Piano;
		Player Kori;

		bool keyDown[255];
		
		int playerMinX;
		int playerMaxX;

		Item Straw;
		Item Choc;
		Item Broc;

		Item Straw2;
		Item Choc2;


		void keyPressed(int key);
		void keyReleased(int key);
		void mousePressed(int x, int y, int button);

		bool Collision(Player* player, Item* item);
		void resetItem(Item* item);
		void drawSkins(Player* player);
		void drawWinner(Player* winner, Player* loser);

		
};
