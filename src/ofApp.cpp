#include "main.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowShape(config::APP_WINDOW_WIDTH, config::APP_WINDOW_HEIGHT);
	ofSetFrameRate(config::APP_DESIRED_FRAMERATE);


	//setup background and fonts
	background.load("bg.png");
	numFont.load("HEARTBRE.TTF", 40);
	textFont.load("Jeepers.ttf", 18);
	scoreBask.load("scorebasket.png");
	logo.load("logo.png");

	pinoCtrls.load("pino controls.png");
	koriCtrls.load("kori controls.png");

	replayBtn.img.load("replay.png");
	replayBtn.x = config::APP_WINDOW_WIDTH / 2;
	replayBtn.y = 830;

	startBtn.img.load("start.png");
	startBtn.x = config::APP_WINDOW_WIDTH / 2;
	startBtn.y = 830;

	//set up players and player controls
	Piano.img.load("pino.png");
	Kori.img.load("korilakkuma.png");

	Piano.expr[0].load("pino happy.png");
	Piano.expr[1].load("pino disgust.png");
	Piano.expr[2].load("pino sad.png");

	Kori.expr[0].load("kori happy.png");
	Kori.expr[1].load("kori disgust.png");
	Kori.expr[2].load("kori sad.png");

	Piano.skins[0].load("Pino glasses.png");
	Piano.skins[1].load("Pino flowers.png");
	Piano.skins[2].load("Pino bow.png");
	Piano.skins[3].load("Pino hat.png");

	Kori.skins[0].load("Korilakkuma glasses.png");
	Kori.skins[1].load("Korilakkuma bow.png");
	Kori.skins[2].load("Korilakkuma backpack.png");
	Kori.skins[3].load("Korilakkuma hat.png");

	ofSetRectMode(OF_RECTMODE_CENTER);

	for (int i = 0; i < 255; i++) {

		keyDown[i] = false;
	}

	Piano.x = config::APP_WINDOW_WIDTH / 2 - 500;
	Piano.y = 800;
	Piano.speed = 8;
	Kori.x = config::APP_WINDOW_WIDTH/2 + 500;
	Kori.y = 800;
	Kori.speed = 8;

	playerMinX = 0 + Piano.img.getWidth()/2;
	playerMaxX = config::APP_WINDOW_WIDTH - Piano.img.getWidth() / 2;

	//setup items
	//strawberry item
	Straw.img.load("strawberry.png");
	Straw.ptValue = 100;
	Straw.speed = 5;

	Straw2.img.load("strawberry.png");
	Straw2.ptValue = 100;
	Straw2.speed = 5;

	//chocolate item
	Choc.img.load("chocolate.png");
	Choc.ptValue = 100;
	Choc.speed = 6;

	Choc2.img.load("chocolate.png");
	Choc2.ptValue = 100;
	Choc2.speed = 6;

	//broccoli item
	Broc.img.load("broccoli.png");
	Broc.ptValue = -100;
	Broc.speed = 4;
	Broc.isBad = true;

	ofSetFrameRate(60);

	Piano.currentSkin = 0;
	Kori.currentSkin = 0;
}

//--------------------------------------------------------------
void ofApp::update(){
	//ez change between game phases for testing
	if (keyDown['1'] == true) {
		Game.gamePhase = 1;
	}
	if (keyDown['2'] == true) {
		Game.gamePhase = 2;
	}
	if (keyDown['3'] == true) {
		Game.gamePhase = 3;
	}

	switch (Game.gamePhase) {
	case 1:
		Piano.x = config::APP_WINDOW_WIDTH / 2 - 500;
		Piano.y = 800;
		Piano.score = 0;
		Kori.x = config::APP_WINDOW_WIDTH / 2 + 500;
		Kori.y = 800;
		Kori.score = 0;
		Broc.y = 0;
		Straw.y = 0;
		Straw2.y = -900;
		Choc.y = 0;
		Choc2.y = -500;

		//Korilakkuma skins
		if (keyDown[OF_KEY_RIGHT] == true && Kori.currentSkin <= 4) {
			Kori.currentSkin++;
		}

		if (keyDown[OF_KEY_LEFT] == true&& Kori.currentSkin >=0) {
			Kori.currentSkin--;
		}

		//Pino skins
		if (keyDown['d'] == true && Piano.currentSkin <= 4) {
			Piano.currentSkin++;
		}

		if (keyDown['a'] == true && Piano.currentSkin >= 0) {
			Piano.currentSkin --;
		}

		break;
	case 2:
		//Korilakkuma controls
		if (keyDown[OF_KEY_RIGHT] == true && Kori.x < playerMaxX) {
			Kori.x += Kori.speed;
		}

		if (keyDown[OF_KEY_LEFT] == true && playerMinX < Kori.x) {
			Kori.x -= Kori.speed;
		}

		//Pino controls
		if (keyDown['d'] == true && Piano.x < playerMaxX) {
			Piano.x += Piano.speed;
		}

		if (keyDown['a'] == true && playerMinX < Piano.x) {
			Piano.x -= Piano.speed;
		}

		//item movement
		Straw.y += Straw.speed;
		Straw2.y += Straw2.speed;
		Choc.y += Choc.speed;
		Choc2.y += Choc2.speed;
		Broc.y += Broc.speed;


		//PLAYER AND OBJECT COLLISIONS
		//PINO COLLISIONS
		if (Collision(&Piano, &Straw)) {
			Piano.colType = 1;
		}

		if (Collision(&Piano, &Straw2)) {
			Piano.colType = 1;
		}

		if (Collision(&Piano, &Choc)) {
			Piano.colType = 1;
		}

		if (Collision(&Piano, &Choc2)) {
			Piano.colType = 1;
		}

		if (Collision(&Piano, &Broc)) {
			Piano.colType = 2;
		}

		//KORI COLLISIONS
		if (Collision(&Kori, &Straw)) {
			Kori.colType = 1;
		}

		if (Collision(&Kori, &Straw2)) {
			Kori.colType = 1;
		}

		if (Collision(&Kori, &Choc)) {
			Kori.colType = 1;
		}

		if (Collision(&Kori, &Choc2)) {
			Kori.colType = 1;
		}

		if (Collision(&Kori, &Broc)) {
			Kori.colType = 2;
		}

		//if the strawberry falls off screen, spawn it back at the top
		resetItem(&Straw);
		resetItem(&Straw2);
		resetItem(&Choc);
		resetItem(&Choc2);
		resetItem(&Broc);

		// game timer
		Game.currentTime = ofGetElapsedTimef();

		Game.timerCtrl(Game.startTime, Game.currentTime);
		break;
	case 3:
		if (Kori.score < Piano.score) {
			Game.winner = 1;
		}
		else if (Kori.score > Piano.score) {
			Game.winner = 2;
		}
		else {
			Game.winner = 3;
		}
		break;
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	background.draw(ofGetWindowWidth() / 2, ofGetWindowHeight() / 2);

	switch (Game.gamePhase) {
	case 1:
		Piano.img.draw(Piano.x, Piano.y);
		Kori.img.draw(Kori.x, Kori.y);

		logo.draw(config::APP_WINDOW_WIDTH / 2, 250);

		pinoCtrls.draw(Piano.x, 720);
		koriCtrls.draw(Kori.x, 720);

		startBtn.img.draw(startBtn.x, startBtn.y);

		ofSetColor(69, 48, 36, 255);
		textFont.drawString("move pino and korilakkuma to collect items\ndifferent items will grant more or less points\n						have fun !!", 700, 650);
		ofSetColor(255);

		drawSkins(&Kori);
		drawSkins(&Piano);

		break;
	case 2:
		Piano.img.draw(Piano.x, Piano.y);
		Kori.img.draw(Kori.x, Kori.y);

		if (Piano.colType == 1) {
			Piano.expr[0].draw(Piano.x, Piano.y);
			if (ofGetElapsedTimeMillis() - Piano.animTime > 500) {
				Piano.colType = 0;
			}
		}
		else if (Piano.colType == 2) {
			Piano.expr[1].draw(Piano.x, Piano.y);
			if (ofGetElapsedTimeMillis() - Piano.animTime > 500) {
				Piano.colType = 0;
			}
		}

		if (Kori.colType == 1) {
			Kori.expr[0].draw(Kori.x, Kori.y);
			if (ofGetElapsedTimeMillis() - Kori.animTime > 500) {
				Kori.colType = 0;
			}
		}
		else if (Kori.colType == 2) {
			Kori.expr[1].draw(Kori.x, Kori.y);
			if (ofGetElapsedTimeMillis() - Kori.animTime > 500) {
				Kori.colType = 0;
			}
		}

		Straw.img.draw(Straw.x, Straw.y);
		Straw2.img.draw(Straw2.x, Straw2.y);
		Choc.img.draw(Choc.x, Choc.y);
		Choc2.img.draw(Choc2.x, Choc2.y);
		Broc.img.draw(Broc.x, Broc.y);

		//draw scores and timer
		scoreBask.draw(165, 115);
		scoreBask.draw(ofGetScreenWidth() - 165, 115);
		ofSetColor(69, 48, 36, 255);
		numFont.drawString(ofToString(Piano.score), 130, 160);
		numFont.drawString(ofToString(Kori.score), ofGetScreenWidth() - 200, 160);

		numFont.drawString(ofToString(Game.timeLeft), ofGetWindowWidth() / 2, 100);
		ofSetColor(255);

		drawSkins(&Kori);
		drawSkins(&Piano);

		break;
	case 3:
		// draw a different scene depending on who wins or if there is a tie
		replayBtn.img.draw(replayBtn.x, replayBtn.y);

		switch (Game.winner) {
		case 1:
			ofSetColor(69, 48, 36, 255);
			numFont.drawString("Pino Wins!", ofGetWindowWidth() / 2 - 100, 150);
			ofSetColor(255);
			drawWinner(&Piano, &Kori);
			break;
		case 2:
			ofSetColor(69, 48, 36, 255);
			numFont.drawString("Korilakkuma Wins!", ofGetWindowWidth() / 2 - 180, 150);
			ofSetColor(255);
			drawWinner(&Kori, &Piano);
			break;
		case 3:
			ofSetColor(69, 48, 36, 255);
			numFont.drawString("Tie!", ofGetWindowWidth() / 2 -20, 300);
			ofSetColor(255);

			ofPushMatrix();
			ofTranslate(ofGetWindowWidth() / 2 - 600, 700);
			ofScale(1.5);
			Piano.x = 0;
			Piano.y = 0;
			Piano.expr[0].draw(0,0);
			drawSkins(&Piano);
			ofPopMatrix();
			
			ofPushMatrix();
			ofTranslate(ofGetWindowWidth() / 2 + 600, 700);
			ofScale(1.5);
			Kori.x = 0;
			Kori.y = 0;
			Kori.expr[0].draw(0,0);
			drawSkins(&Kori);
			ofPopMatrix();

			scoreBask.draw(ofGetWindowWidth()/2, 600);
			ofSetColor(69, 48, 36, 255);
			numFont.drawString(ofToString(Piano.score), ofGetWindowWidth() / 2 - 40, 650);
			ofSetColor(255);
			
			break;
		}
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	keyDown[key] = true;

	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	keyDown[key] = false;
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	// click on start button
	if (Game.gamePhase == 1 &&
		x > startBtn.x - startBtn.img.getWidth()/2 && x < startBtn.x + startBtn.img.getWidth() / 2 &&
		y > startBtn.y - startBtn.img.getHeight() / 2 && y < startBtn.y + startBtn.img.getHeight() / 2) {
		Game.gamePhase = 2;
		Game.setup();
	}

	//click on replay button
	if (Game.gamePhase == 3 &&
		x > replayBtn.x - replayBtn.img.getWidth() / 2 && x < replayBtn.x + replayBtn.img.getWidth() / 2 &&
		y > replayBtn.y - replayBtn.img.getHeight() / 2 && y < replayBtn.y + replayBtn.img.getHeight() / 2) {
		Game.gamePhase = 1;
	}
}

//checks for collision between player and item and changes score value and item position accordingly
bool ofApp::Collision(Player* player, Item* item) {
	int tempY = 0;

	if (item->y < player->y + player->img.getHeight() && item->y > player->y) {
		if (item->x < player->x + player->img.getWidth() / 2 && item->x > player->x - player->img.getWidth() / 2) {
			player->score += item->ptValue;
			if (player->score < 0) {
				player->score = 0;
			}
			tempY = config::APP_WINDOW_HEIGHT - item->y;

			//if the item is caught, make the poistion higher than zero to allow for buffer time
			item->randPos();
			item->y -= tempY;
			player->animTime = ofGetElapsedTimeMillis();
			return true;
		}
	}
	return false;
}

void ofApp::resetItem(Item* item) {
	if (item->y > config::APP_WINDOW_HEIGHT + item->img.getHeight() / 2) {
		item->randPos();
	}
}

void ofApp::drawWinner(Player* winner, Player* loser) {
	ofPushMatrix();
	ofTranslate(ofGetWindowWidth()/2, ofGetWindowHeight() / 2 - 100);
	ofScale(2.0f);
		winner->x = 0;
		winner->y = 0;
		winner->expr[0].draw(0,0);
		drawSkins(winner);
	ofPopMatrix();

	scoreBask.draw(1200, 600);
	ofSetColor(69, 48, 36, 255);
	numFont.drawString(ofToString(winner->score), 1150, 650);
	ofSetColor(255);

	ofPushMatrix();
	ofTranslate(ofGetWindowWidth() / 2, ofGetWindowHeight() / 2);
		loser->x = 600;
		loser->y = 90;

		loser->expr[2].draw(600, 90);
		drawSkins(loser);
	ofPopMatrix();
	ofPushMatrix();
	ofTranslate(ofGetWindowWidth()/2 + 750, ofGetWindowHeight()/2 + 180);
	ofScale(0.5f);
		scoreBask.draw(0, 0);
		ofSetColor(69, 48, 36, 255);
		numFont.drawString(ofToString(loser->score), -50, 50);
		ofSetColor(255);
	ofPopMatrix();
}

void ofApp::drawSkins(Player* player) {
	//pino skins
	if (player == &Piano) {
		switch (Piano.currentSkin) {
		case 1:
			Piano.skins[0].draw(Piano.x, Piano.y);
			break;
		case 2:
			Piano.skins[1].draw(Piano.x, Piano.y);
			break;
		case 3:
			Piano.skins[2].draw(Piano.x, Piano.y);
			break;
		case 4:
			Piano.skins[3].draw(Piano.x, Piano.y);
			break;
		default:
			break;
		}
	}

	//korilakkuma skins
	if (player == &Kori) {
		switch (Kori.currentSkin) {
		case 1:
			Kori.skins[0].draw(Kori.x, Kori.y);
			break;
		case 2:
			Kori.skins[1].draw(Kori.x, Kori.y);
			break;
		case 3:
			Kori.skins[2].draw(Kori.x, Kori.y);
			break;
		case 4:
			Kori.skins[3].draw(Kori.x, Kori.y);
			break;
		default:
			break;
		}
	}
}