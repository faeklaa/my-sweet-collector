#pragma once

#include "main.h"
#include "ofMain.h"
#include "GameController.h"
#include "GameObject.h"
#include "Player.h"
#include "Item.h"
#include "ofApp.h"

namespace config{
	static const int APP_WINDOW_WIDTH = 1920;
	static const int APP_WINDOW_HEIGHT = 1080;
	static const int APP_DESIRED_FRAMERATE = 60;
}