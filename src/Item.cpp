#include "main.h"

Item::Item() {
	ptValue = 0;
	spawnTime = 0;

	//randomaize a position upon first spawn
	y = 0;
	x = rand() % (config::APP_WINDOW_WIDTH + 1);

	isCaught = false;
	isBad = false;
}

void Item::randPos() {
	y = 0;
	x = rand() % (config::APP_WINDOW_WIDTH + 1);
}
