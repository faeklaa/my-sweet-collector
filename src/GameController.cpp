#include "main.h"

GameController::GameController() {
	totalTime = 60;
	gamePhase = 1;
	winner = 0;
}

void GameController::timerCtrl(int start, int current) {

	current = ofGetElapsedTimef(); //this will go into if statement once game phases are done
	timePassed = current - start;
	timeLeft = totalTime - timePassed;


	if (gamePhase == 2) {
		if (timeLeft == 0) { //once timer runs out, moves onto final win phase
			gamePhase = 3;
		}
	}
}

void GameController::setup() {
	startTime = ofGetElapsedTimef();
}