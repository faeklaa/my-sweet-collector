#pragma once
class GameController
{
public:
	int totalTime;
	int startTime;
	int currentTime;
	int timePassed;
	int timeLeft;

	int gamePhase;
	int winner; //1 for pino, 2 for korillakuma, 3 for tie

	GameController();
	void timerCtrl(int start, int current);
	void setup();
};

